<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WallRecordImage
 *
 * @property integer    id
 * @property WallRecord post
 * @property Carbon     created_at
 * @property Carbon     updated_at
 * @property Carbon     deleted_at
 * @package App\Models
 */
class WallRecordImage extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //

    #region relation
    public function wallRecord()
    {
        return $this->belongsTo(WallRecord::class);
    }
    #endregion

    #region scope
    #endregion

    #region methods
    #endregion

    #region append attributes
    #endregion
}

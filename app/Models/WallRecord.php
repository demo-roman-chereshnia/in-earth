<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WallRecord
 *
 * @property integer    id
 * @property User       user
 * @property string     text
 * @property Carbon     created_at
 * @property Carbon     updated_at
 * @property Carbon     deleted_at
 * @property Carbon     dateTouch
 * @property Collection likeUsers
 *
 * @package App\Models
 */
class WallRecord extends Model
{
    use SoftDeletes;
    protected $with = ['likeUsers'];
    protected $dates = ['deleted_at'];
    //
    protected $fillable = ['text'];
    protected $visible = ['id', 'user', 'content', 'comments', 'likeUserIds', 'date'];
    protected $hidden = ['images', 'text'];
    protected $appends = ['user', 'content', 'likeUserIds', 'date', 'comments'];
    protected $casts = [];

    #region relation
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function wallRecordsNotInterest()
    {
        return $this->belongsToMany(User::class, 'wall_record_user_exclude');
    }

    public function images()
    {
        return $this->hasMany(WallRecordImage::class);
    }

    public function commentsRel()
    {
        return $this->hasMany(Comment::class, 'wall_record_id');
    }

    public function likeUsers()
    {
        return $this->belongsToMany(User::class, 'wall_record_likes');
    }
    #endregion

    #region scope
    #endregion

    #region append attributes
    public function getCommentsAttribute()
    {
        return $this->commentsRel->count() ? Comment::prepareCommentsAsObjects($this->commentsRel)->values() : collect([]);
    }

    public function getUserAttribute()
    {
        return $this->author;
    }

    public function getContentAttribute()
    {
        return [
            "text" => $this->text,
            "images" => $this->images->pluck('path'),
        ];
    }

    public function getLikesAttribute()
    {
        return $this->likeUsers;
    }

    public function getDateAttribute()
    {
        return [
            "created" => $this->created_at ? $this->created_at->timestamp : null,
            "updated" => $this->updated_at ? $this->updated_at->timestamp : null,
        ];
    }

    public function getDateTouchAttribute()
    {
        return $this->updated_at ? $this->updated_at : $this->created_at;
    }

    public function getLikeUserIdsAttribute()
    {
        return $this->likeUsers->pluck('id');
    }
    #endregion

    #region methods
    public function hasUserLike(User $user)
    {
        return $this->likeUsers()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Is $user is
     *
     * @param User $user
     * @return bool
     */
    public function isAuthor(User $user)
    {
        return $this->user_id == $user->id;
    }
    #endregion
}

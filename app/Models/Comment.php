<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer    id
 * @property User       user
 * @property string     text
 * @property Carbon     created_at
 * @property Carbon     updated_at
 * @property Carbon     deleted_at
 * @property Collection likeUsers
 */
class Comment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /** @var  Collection */
    public $childs;
    protected $table = "wall_comments";
    //
    protected $fillable = ['text'];
    protected $visible = ['id', 'user', 'text', 'likeUserIds', 'date', 'comments', 'wall_record_id', 'parent_id'];
    protected $hidden = [];
    protected $appends = ['childs', 'user', 'likeUserIds', 'date', 'comments'];
    protected $casts = [];

    public function pushChild(Comment $comment)
    {
        if (!$comment) return;

        if (empty($this->childs)) {
            $this->childs = collect([]);
        }

        $this->childs->push($comment);
    }

    #region relation
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function commentsRel()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function likeUsers()
    {
        return $this->belongsToMany(User::class, 'wall_comment_likes', 'wall_comment_id');
    }
    #endregion

    #region scope
    #endregion

    #region append attributes
    public function getChildsAttribute()
    {
        return $this->childs ?: collect([]);
    }

    public function getUserAttribute()
    {
        return $this->author;
    }

    public function getLikesAttribute()
    {
        return $this->likeUsers;
    }

    public function getDateAttribute()
    {
        return [
            "created" => $this->created_at ? $this->created_at->timestamp : null,
            "updated" => $this->updated_at ? $this->updated_at->timestamp : null,
        ];
    }

    public function getCommentsAttribute()
    {
        return $this->childs ?: collect([]);
    }

    public function getLikeUserIdsAttribute()
    {
        return $this->likeUsers->pluck('id');
    }
    #endregion

    #region methods

    public function hasUserLike(User $user)
    {
        return $this->likeUsers()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Is $user is
     *
     * @param User $user
     * @return bool
     */
    public function isAuthor($user)
    {
        return $this->user_id == $user->id;
    }

    public static function prepareCommentsAsObjects($comments, $addChildToParent = true)
    {
        if (!$addChildToParent) {
            return $comments;
        }

        $newComments = collect([]);

        foreach ($comments as $comment) {
            $newComments->put($comment->id, $comment);
        }

        foreach ($newComments as $index => &$v) {
            if ($v->parent_id != 0) {

                /** @var Comment $item */
                $item = $newComments->get($v->parent_id);

                if ($item) {
                    $item->pushChild($v);
                    $newComments->put($v->parent_id, $item);
                }
            }
        }

        unset($v);

        foreach ($newComments as $k => $v) {
            if ($v->parent_id != 0) {
                $newComments->pull($k);
            }
        }

        return $newComments;
    }

    public function isRelateToRecord(WallRecord $wallRecord)
    {
        return $this->wall_record_id === $wallRecord->id;
    }

    public function setParent(Comment $comment = null)
    {
        if ($comment)
            $this->parent_id = $comment->id;
        else
            $this->parent_id = null;
    }

    public function setWallRecord(WallRecord $wallRecord)
    {
        $this->wall_record_id = $wallRecord->id;
    }
    #endregion
}

<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 09.11.16
 * Time: 7:13 PM
 */

namespace App\Repository;

use App\Events\WallRecordWasCreated;
use App\Events\WallRecordWasDeleted;
use App\Events\WallRecordWasLiked;
use App\Events\WallRecordWasUpdated;
use App\Models\WallRecord;
use App\User;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;

class WallRepository
{
    /**
     * @var WallRecord
     */
    private $wallRecord;

    public function __construct(WallRecord $wallRecord)
    {
        $this->wallRecord = $wallRecord;
    }

    /**
     * Retrive query of select list wall records
     *
     * @param Authenticatable $user
     * @param null            $order
     * @return Builder
     */
    protected function getListQuery(Authenticatable $user, $order = null)
    {
        $userId = $user->getAuthIdentifier();

        /** @var Builder $recordsQuery */
        $recordsQuery =
            $this->wallRecord
                ->orderBy('created_at', 'desc')
                ->with([
                    'author', 'images', 'commentsRel' => function ($query) use ($userId) {
                        $query
                            ->orderBy('created_at', 'asc')
                            ->withCount('likeUsers')
                            ->withCount([
                                'likeUsers as likeByUser' => function ($query) use ($userId) {
                                    $query->where('wall_comment_likes.user_id', $userId);
                                },
                            ]);
                    },
                ])
                ->withCount('likeUsers')
                ->withCount([
                    'likeUsers as likeByUser' => function ($query) use ($userId) {
                        $query->where('wall_record_likes.user_id', $userId);
                    },
                ]);

        $recordsQuery = $recordsQuery->whereDoesntHave('wallRecordsNotInterest', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        });

        if ($order === 'like') {
            $recordsQuery->orderBy('like_users_count', 'desc');
        } else {
            $recordsQuery->orderBy('created_at', 'desc');
        }

        $recordsQuery->orderBy('id', 'desc');

        return $recordsQuery;
    }

    /**
     * Get all list wall records
     *
     * @param Authenticatable $user
     * @param null            $order
     * @return Builder
     */
    public function getListAll(Authenticatable $user, $order = null)
    {
        return $this->getListQuery($user, $order);
    }

    /**
     * Get pagginabel list of wall records
     *
     * @param Authenticatable $user
     * @param int             $page
     * @param int             $countPerPage
     * @param null            $order
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getListPaginable(Authenticatable $user, $page = 1, $countPerPage = 0, $order = null)
    {
        $records = $this->getListQuery($user, $order);

        return $records->paginate($countPerPage, ['*'], 'page', $page);
    }

    /**
     * Create wall record for $user
     *
     * @param User       $user
     * @param WallRecord $wallRecord
     * @return WallRecord
     */
    public function createRecord(User $user, WallRecord $wallRecord)
    {
        $wallRecord->updated_at = null;
        $user->myAuthorshipWallRecords()->save($wallRecord);

        // Or can use ...
        // $post->author()->associate($request->user());
        // $post->save();

        broadcast(new WallRecordWasCreated($wallRecord))->toOthers();

        return $wallRecord;
    }

    /**
     * Remove wall record
     *
     * @param WallRecord $wallRecord
     * @return bool|null
     */
    public function removeRecord(WallRecord $wallRecord)
    {
        $delete = $wallRecord->delete();

        broadcast(new WallRecordWasDeleted($wallRecord))->toOthers();

        return $delete;
    }

    /**
     * Update wall record
     *
     * @param WallRecord $wallRecord
     * @return bool
     */
    public function updateRecord(WallRecord $wallRecord)
    {
        if (!$wallRecord->exists) {
            return false;
        }

        $save = $wallRecord->save();

        broadcast(new WallRecordWasUpdated($wallRecord))->toOthers();

        return $save;
    }

    /**
     * Add wall record to exclude list of records on $user
     *
     * @param User       $user
     * @param WallRecord $wallRecord
     */
    public function addToExcludedRecordsOnUser(User $user, WallRecord $wallRecord)
    {
        $user->wallRecordsNotInterest()->attach($wallRecord->id);

        $this->unCheckLike($user, $wallRecord);
    }

    /**
     * Toggle user like on wall record
     *
     * @param User       $user
     * @param WallRecord $wallRecord
     * @return int Current-New state
     * @throws Exception
     */
    public function toggleLikeWallRecordByUser(User $user, WallRecord $wallRecord)
    {
        if ($wallRecord->isAuthor($user)) {
            throw new Exception("Author can't like own record");
        }

        if ($wallRecord->hasUserLike($user)) {
            return $this->unCheckLike($user, $wallRecord);
        }

        return $this->checkLike($user, $wallRecord);
    }

    private function unCheckLike(User $user, WallRecord $wallRecord)
    {
        $wallRecord->likeUsers()->detach($user->id);

        broadcast(new WallRecordWasLiked($wallRecord, $user, false))->toOthers();
    }

    private function checkLike(User $user, WallRecord $wallRecord)
    {
        $wallRecord->likeUsers()->attach($user->id);

        broadcast(new WallRecordWasLiked($wallRecord, $user, true))->toOthers();
    }
}
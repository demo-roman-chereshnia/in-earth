<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 16.11.16
 * Time: 1:18 PM
 */

namespace App\Repository;

use App\Models\Comment;
use App\Models\WallRecord;
use App\User;
use Exception;

class WallCommentRepository
{
    /**
     * Create wall comment
     *
     * @param User       $user
     * @param WallRecord $wallRecord
     * @param Comment    $comment
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createComment(Comment $comment, User $user, WallRecord $wallRecord, Comment $wallComment = null)
    {
        $comment->setWallRecord($wallRecord);

        if ($wallComment->exists) {
            $comment->setParent($wallComment);
        }

        $save = $user->commentsRel()->save($comment);

        // todo: place broadcast or/and fire event

        return $save;
    }

    /**
     * Remove wall comment
     *
     * @param WallRecord $wallRecord
     * @param Comment    $wallComment
     * @return bool|null
     */
    public function removeComment(WallRecord $wallRecord, Comment $wallComment)
    {
        $delete = $wallComment->delete();

        // todo: place broadcast or/and fire event

        return $delete;
    }

    public function updateComment()
    {
        throw new \Exception("Not implemented yet");
    }

    /**
     * Toggle user like on wall comment
     *
     * @param User    $user
     * @param Comment $comment
     * @return bool
     * @throws Exception
     */
    public function toggleLikeCommentByUser(User $user, Comment $comment)
    {
        if ($comment->isAuthor($user)) {
            throw new Exception("Author can't like own comment");
        }

        if ($oldState = $comment->hasUserLike($user)) {
            $comment->likeUsers()->detach($user->id);
        } else {
            $comment->likeUsers()->attach($user->id);
        }

        return $comment->hasUserLike($user);
    }
}
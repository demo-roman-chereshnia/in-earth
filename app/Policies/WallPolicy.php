<?php

namespace App\Policies;

use App\Models\WallRecord;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class WallPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create WallRecords.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        // todo : move to config file
        $limitSeconds = env('LIMIT_SECONDS_RECORD_CREATE', 1);

        $lastPost = $user->lastPost()->first();

        if ($lastPost) {
            return $limitSeconds < $lastPost->dateTouch->diffInSeconds(Carbon::now());
        }

        return true;
    }

    /**
     * Determine whether the user can update the WallRecord.
     *
     * @param  \App\User  $user
     * @param  WallRecord $wallRecord
     * @return mixed
     */
    public function update(User $user, WallRecord $wallRecord)
    {
        return $wallRecord->isAuthor($user);
    }

    /**
     * Determine whether the user can delete the WallRecord.
     *
     * @param  \App\User  $user
     * @param  WallRecord $wallRecord
     * @return mixed
     */
    public function delete(User $user, WallRecord $wallRecord)
    {
        return $wallRecord->isAuthor($user);
    }

    /**
     * Determine whether the user can make record `not interest`.
     * Only not owner can hide wall record.
     *
     * @param  \App\User  $user
     * @param  WallRecord $wallRecord
     * @return mixed
     */
    public function canMakeNotInterest(User $user, WallRecord $wallRecord)
    {
        return !$wallRecord->isAuthor($user);
    }

    /**
     * Determine whether the user can like wall record.
     * Only not owner can like wall record.
     *
     * @param  \App\User  $user
     * @param  WallRecord $wallRecord
     * @return mixed
     */
    public function canLike(User $user, WallRecord $wallRecord)
    {
        return !$wallRecord->isAuthor($user);
    }
}

<?php

namespace App\Policies;

use App\Models\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        throw new \Exception("Not implemented yet");
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\User $user
     * @param  Comment   $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        return $comment->isAuthor($user);
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\User $user
     * @param  Comment   $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        return $comment->isAuthor($user);
    }

    /**
     * Determine whether the user can like comment.
     * Only not owner can like comment.
     *
     * @param  \App\User $user
     * @param Comment    $comment
     * @return mixed
     */
    public function canLike(User $user, Comment $comment)
    {
        return !$comment->isAuthor($user);
    }
}

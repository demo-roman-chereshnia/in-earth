<?php

namespace App\Providers;

use App\Models\WallRecord;
use App\Routing\ExtendResponseFactory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->instance(
            \App\Repository\WallRepository::class,
            new \App\Repository\WallRepository(new WallRecord())
        );

        // TODO: find a better solution
        // Overwrite core ResponseFactory
        $this->app->singleton('Illuminate\Contracts\Routing\ResponseFactory', function ($app) {
            return new ExtendResponseFactory($app['Illuminate\Contracts\View\Factory'], $app['redirect']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 12.11.16
 * Time: 4:30 PM
 */

namespace App\Routing;

use App\Contracts\Routing\IExtendResponseFactory;
use Illuminate\Routing\ResponseFactory;

class ExtendResponseFactory extends ResponseFactory implements IExtendResponseFactory
{
    # success
    const SUCCESS_GENERAL = "Success operation";
    const SUCCESS_CREATED = "Node created success";
    const SUCCESS_DELETED = "Node deleted success";
    const SUCCESS_UPDATED = "Node updated success";
    # fail
    const FAIL_GENERAL = "Operation failed";
    const FAIL_VALIDATION = "Validation failed";
    const FAIL_NOT_FOUND = "Not found";
    const FAIL_ACCESS_DENIED = "Forbidden! Access denied ;(";

    public function created($data = [], $message = '', array $headers = [])
    {
        return $this->success($data, $message ?: self::SUCCESS_CREATED, 201, $headers);
    }

    public function deleted($data = [], $message = '', array $headers = [])
    {
        return $this->success($data, $message ?: self::SUCCESS_DELETED, 200, $headers);
    }

    public function updated($data = [], $message = '', array $headers = [])
    {
        return $this->success($data, $message ?: self::SUCCESS_UPDATED, 200, $headers);
    }

    public function success($data = [], $message = '', $httpCode = 200, array $headers = [])
    {
        return $this->json([
            "success" => true,
            "code" => $httpCode,
            "message" => $message ?: self::SUCCESS_GENERAL,
            "data" => !empty($data) ? $data : (object)[],
        ], $httpCode, $headers);
    }

    public function failValidation($data = [], $message = '', array $headers = [])
    {
        return $this->fail($data, $message ?: self::FAIL_VALIDATION, 422, $headers);
    }

    public function notFound($data = [], $message = '', array $headers = [])
    {
        return $this->fail($data, $message ?: self::FAIL_NOT_FOUND, 404, $headers);
    }

    public function accessDenied($data = [], $message = '', $httpCode = 403, array $headers = [])
    {
        return $this->fail($data, $message ?: self::FAIL_ACCESS_DENIED, $httpCode ?: 403, $headers);
    }

    public function fail($data = [], $message = '', $httpCode = 400, array $headers = [])
    {
        $errors = !empty($data) ? $data : (object)[];

        return $this->json([
            "success" => false,
            "code" => $httpCode,
            "message" => $message ?: self::FAIL_GENERAL,
            "errors" => $errors,
        ], $httpCode, $headers);
    }
}
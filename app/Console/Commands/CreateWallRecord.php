<?php

namespace App\Console\Commands;

use App\Models\WallRecord;
use App\Repository\WallRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateWallRecord extends Command
{
    protected $signature = 'wall:create-record {--message=} {--user=} {--now=false}';
    protected $description = 'Create Wall Record by random/specific user';

    public function handle(WallRepository $wall)
    {
        $data = [];

        if ($this->hasOption('message') && $this->option('message')) {
            array_set($data, 'text', $this->option('message'));
        }

        if ($this->hasOption('user') && $this->option('user')) {
            $user = User::findOrFail($this->option('user'));
        } else {
            $user = User::orderBy(DB::raw('RAND()'))->first();
        }

        if ($this->hasOption('now') && $this->option('now')) {
            array_set($data, 'created_at', Carbon::now());
            array_set($data, 'updated_at', null);
        }

        $wall->createRecord($user, factory(WallRecord::class)->make($data));
    }
}

<?php

namespace App\Listeners;

use App\Events\WallRecordCreated;
use App\Events\WallRecordWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WallRecordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WallRecordWasCreated $event
     * @return void
     */
    public function handle(WallRecordWasCreated $event)
    {
        //
    }
}

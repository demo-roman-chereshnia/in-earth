<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\WallCommentRequest;
use App\Models\Comment;
use App\Models\WallRecord;
use App\Repository\WallCommentRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class WallCommentApiController extends ApiController
{
    /**
     * Check relation wall record with wall comment.
     *
     * @param WallRecord $record
     * @param Comment    $comment
     * @throws Exception
     */
    private function checkRelationComment(WallRecord $record, Comment $comment)
    {
        if (!$comment->isRelateToRecord($record)) {
            throw new Exception("Comment {$comment->id} not related to record {$record->id}");
        }
    }

    public function createWallComment(
        WallCommentRequest $request, WallRecord $record, Comment $comment, WallCommentRepository $commentRepository
    )
    {
        $comment->exists && $this->checkRelationComment($record, $comment);

        $instance = $comment->newInstance($request->all());

        $commentRepository->createComment($instance, $request->user(), $record, $comment);

        /** Fail case catch by App Handler */

        return $this->response()->created($instance);
    }

    public function deleteWallComment(Request $request, WallRecord $record, Comment $comment, WallCommentRepository $commentRepository)
    {
        if (Gate::denies('delete', $comment)) {
            return $this->response()->accessDenied([
                "cause" => "You are not the author",
            ]);
        }

        $this->checkRelationComment($record, $comment);

        if ($commentRepository->removeComment($record, $comment) === false) {
            throw new Exception("Comment wasn't deleted");
        }

        return $this->response()->deleted($comment);
    }

    public function updateWallComment()
    {
        throw new Exception("Not implemented yet");
    }

    public function likeToggleWallComment(Request $request, WallRecord $record, Comment $comment, WallCommentRepository $commentRepository)
    {
        if (Gate::denies('canLike', $comment)) {
            return $this->response()->accessDenied([
                "cause" => "You are the author of comment",
            ]);
        }

        if ($commentRepository->toggleLikeCommentByUser($request->user(), $comment)) {
            $message = "Comment #{$comment->id} was liked";
        } else {
            $message = "Comment #{$comment->id} was unliked";
        }

        return $this->response()->success([
            "cause" => $message,
        ]);
    }
}
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\WallRecordRequest;
use App\Models\WallRecord;
use App\Repository\WallRepository;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class WallApiController extends ApiController
{
    public function getWallRecords(Request $request, WallRepository $wall, Authenticatable $user)
    {
        // $page = $request->get('page', 1);
        // $countPerPage = config('pagination.perPage');
        $order = $request->get('order', 'created');

        /** @var Paginator $posts */
        // $posts = $wall->getListPaginable($user, $page, $countPerPage, $order);

        $posts = $wall->getListAll($user, $order)->get();

        return $this->response()->success([
            "countTotal" => $posts->count(),
            "items" => $posts,
        ]);
    }

    public function createWallRecord(WallRecordRequest $request, WallRepository $wall, WallRecord $wallRecord)
    {
        if (Gate::denies('create', $wallRecord)) {

            // todo : move to config file
            $countLimit = env('LIMIT_SECONDS_RECORD_CREATE', 1);

            return $this->response()->accessDenied([
                "cause" => "Not more than {$countLimit} seconds, you can create record!",
            ]);
        }

        $post = $wall->createRecord($request->user(), $wallRecord->newInstance()->fill($request->all()));

        /** Fail case catch by App Handler */
        return $this->response()->created($post);
    }

    public function deleteWallRecord(WallRecord $wallRecord, WallRepository $wall, Authenticatable $user)
    {
        if (Gate::denies('delete', $wallRecord)) {
            return $this->response()->accessDenied([
                "cause" => "You are not the author",
            ]);
        }

        if ($wall->removeRecord($wallRecord) === false) {
            throw new Exception("Record wasn't deleted");
        }

        return $this->response()->deleted($wallRecord);
    }

    public function updateWallRecord(WallRecordRequest $request, WallRepository $wall, WallRecord $wallRecord)
    {
        if (Gate::denies('update', $wallRecord)) {
            return $this->response()->accessDenied([
                "cause" => "You are not the author",
            ]);
        }

        $update = $wall->updateRecord($wallRecord->fill($request->all()));

        if ($update === false) {
            throw new Exception("Record wasn't updated");
        }

        return $this->response()->updated($wallRecord);
    }

    public function notInterestWallRecord(Request $request, WallRecord $wallRecord, WallRepository $wall)
    {
        if (Gate::denies('canMakeNotInterest', $wallRecord)) {
            return $this->response()->accessDenied([
                "cause" => "You are the author of wall record",
            ]);
        }

        $wall->addToExcludedRecordsOnUser($request->user(), $wallRecord);

        return $this->response()->success([
            "cause" => "Wall record #{$wallRecord->id} was success hide",
        ]);
    }

    public function likeToggleWallRecord(Request $request, WallRecord $wallRecord, WallRepository $wall)
    {
        if (Gate::denies('canLike', $wallRecord)) {
            return $this->response()->accessDenied([
                "cause" => "You are the author of wall record",
            ]);
        }

        if ($state = $wall->toggleLikeWallRecordByUser($request->user(), $wallRecord)) {
            $message = "Wall record #{$wallRecord->id} was liked";
        } else {
            $message = "Wall record #{$wallRecord->id} was unliked";
        }

        return $this->response()->success([
            "likeUserIds" => $wallRecord->likeUserIds,
            "state" => $state,
            "cause" => $message,
        ]);
    }
}


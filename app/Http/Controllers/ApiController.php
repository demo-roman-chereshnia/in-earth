<?php

namespace App\Http\Controllers;

use App\Contracts\Routing\IExtendResponseFactory;

/**
 * Class ApiController
 * Declare specific wrapper for api response
 *
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory|IExtendResponseFactory
     */
    public function response()
    {
        return response();
    }
}

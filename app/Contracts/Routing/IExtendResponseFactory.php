<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 12.11.16
 * Time: 4:37 PM
 */

namespace App\Contracts\Routing;

interface  IExtendResponseFactory
{
    public function created($data = [], $message = '', array $headers = []);

    public function deleted($data = [], $message = '', array $headers = []);

    public function updated($data = [], $message = '', array $headers = []);

    public function success($data = [], $message = '', $httpCode = 200, array $headers = []);

    public function failValidation($data = [], $message = '', array $headers = []);

    public function notFound($data = [], $message = '', array $headers = []);

    public function fail($data = [], $message = '', $httpCode = 422, array $headers = []);

    public function accessDenied($data = [], $message = '', $httpCode = 403, array $headers = []);
}
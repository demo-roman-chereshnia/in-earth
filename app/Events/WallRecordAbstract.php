<?php

namespace App\Events;

use App\Models\WallRecord;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

abstract class WallRecordAbstract implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
    /**
     * @var WallRecord
     */
    public $wallRecord;

    /**
     * Create a new event instance.
     *
     * @param WallRecord $wallRecord
     */
    public function __construct(WallRecord $wallRecord)
    {
        //
        $this->wallRecord = $wallRecord;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('wall-records');
    }
}

<?php

namespace App\Events;

use App\Models\WallRecord;
use App\User;

class WallRecordWasLiked extends WallRecordAbstract
{
    /**
     * @var User
     */
    public $user;
    /**
     * @var boolean|null
     */
    public $state;

    /**
     * WallRecordWasLiked constructor.
     *
     * @param WallRecord $wallRecord
     * @param User       $user
     * @param null       $state Like on event fire or Unlike. Null unknown
     */
    public function __construct(WallRecord $wallRecord, User $user = null, $state = null)
    {
        parent::__construct($wallRecord);

        $this->user = $user;
        $this->state = (bool)$state;
    }
}
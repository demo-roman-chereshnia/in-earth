<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 12.11.16
 * Time: 9:08 PM
 */

namespace App\Exceptions;

use App\Contracts\Routing\IExtendResponseFactory;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

trait RestExceptionHandlerTrait
{
    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request   $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, Exception $e)
    {
        switch (true) {
            case $this->isModelNotFoundException($e):
                $retval = $this->modelNotFound();
                break;
            case $this->isValidationException($e):
                $retval = $this->response()->failValidation($e->response->getData());
                break;
            default:

                if (config('app.debug')) {
                    return $this->showException($e);
                }

                $retval = $this->badRequest();
        }

        return $retval;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory|IExtendResponseFactory
     */
    protected function response()
    {
        return response();
    }

    /**
     * Returns json response for Eloquent model not found exception.
     *
     * @param string $message
     * @param int    $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function modelNotFound($message = 'Record not found', $statusCode = 404)
    {
        return $this->response()->notFound();
    }

    /**
     * Returns json response for generic bad request.
     *
     * @param string $message
     * @param int    $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function badRequest($message = 'Bad request', $statusCode = 400)
    {
        return $this->response()->fail(['cause' => $message], null, $statusCode);
    }

    /**
     * Return json response for any input exception
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showException(Exception $e)
    {
        return $this->response()->fail([
            "cause" => "Exception <" . get_class($e) . ">",
            "code" => $e->getCode(),
            "massage" => $e->getMessage(),
            "line" => $e->getLine(),
            "file" => $e->getFile(),

            "trace" => $e->getTrace(),
        ]);
    }

    /**
     * Determines if the given exception is an Eloquent model not found.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isModelNotFoundException(Exception $e)
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * Determines id the given exception is an Validation exception
     *
     * @param Exception $e
     * @return bool
     */
    protected function isValidationException(Exception $e)
    {
        return $e instanceof ValidationException;
    }
}
<?php

namespace App;

use App\Models\Comment;
use App\Models\WallRecord;
use Creativeorange\Gravatar\Facades\Gravatar;
use Exception;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property WallRecord|null lastPost
 */
class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'avatar', 'gravatar', 'email', 'password',
    ];
    protected $visible = [
        'id', 'name', 'avatarUrl',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['avatarUrl'];

    #region relation
    public function myAuthorshipWallRecords()
    {
        return $this->hasMany(WallRecord::class);
    }

    public function wallRecordsNotInterest()
    {
        return $this->belongsToMany(WallRecord::class, 'wall_record_user_exclude');
    }

    public function commentsRel()
    {
        return $this->hasMany(Comment::class);
    }

    public function lastPost()
    {
        return $this->hasOne(WallRecord::class)->orderBy('created_at', 'desc');
    }
    #endregion

    #region scope
    #endregion

    #region methods
    #endregion

    public function getAvatarUrlAttribute()
    {
        if ($this->gravatar) {

            return $this->urlGravatar($this->attributes['email']);
        }

        $imageName = $this->attributes['avatar'];

        empty($imageName) && $imageName = "default-one.png";

        return url("/media/avatar/large/$imageName");
    }

    protected function urlGravatar($email)
    {
        return Gravatar::get($email);
    }
}

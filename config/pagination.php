<?php
/**
 * Created by PhpStorm.
 * User: smuchka
 * Date: 10.11.16
 * Time: 3:23 AM
 */

return [
    'perPage' => env('PAGINATION_PER_PAGE'),
];
const
    elixir = require('laravel-elixir'),
    gulp = require('gulp');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss')
        .webpack('app.js');
});


gulp.task('publish_Semantic', function () {
    gulp
        .src('./bower_components/semantic/dist/*.*')
        .pipe(gulp.dest('./public/libs/semantic'));
    gulp
        .src('./bower_components/semantic/dist/themes/default/assets/fonts/*.*')
        .pipe(gulp.dest('./public/libs/semantic/themes/default/assets/fonts'));
});

gulp.task('publish_moment', function () {
    gulp
        .src('./bower_components/moment/moment.js')
        .pipe(gulp.dest('./public/libs/moment'));
});

// gulp.task('publish_dropzone', function () {
//     gulp
//         .src(['./node_modules/dropzone/dist/min/*.*'])
//         .pipe(gulp.dest('./public/libs/dropzone/'));
// });

gulp.task('publish', [
    'publish_Semantic',
    'publish_moment'
]);

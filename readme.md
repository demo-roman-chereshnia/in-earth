# Laravel PHP Framework

## Install application
   
* ```cd ./in-earth ```
* ```composer install```
   
* install gulp as global ```npm install --g gulp-cli```
* install all npm dependencies ``` npm install ```

* install bower ```npm install -g bower```
* install all bower dependencies ```bower install (bower install --allow-root)```

## Config laravel app

* ```cp .env.example .env```
* ```php artisan key:generate```
   
##   ! database: migration + seeds

*  ```php artisan migrate --seed```
    
## ! note permissions
* ```sudo chgrp -R www-data storage bootstrap/cache```
* ```sudo chmod -R ug+rwx storage bootstrap/cache```
   
## ! update .env file

* set ```PUSHER_APP_ID=```
* set ```PUSHER_KEY=```
* set ```PUSHER_SECRET=``` 
   
## BEFORE UP APP
* ```gulp``` 
* ```gulp publish``` 
* ```php artisan route:clear``` 
* ```php artisan cache:clear``` 
* ```php artisan config:clear``` 
* ```php artisan optimize``` 

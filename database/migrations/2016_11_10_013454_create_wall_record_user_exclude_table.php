<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWallRecordUserExcludeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wall_record_user_exclude', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('wall_record_id')->unsigned();

            $table->timestamps = false;

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('wall_record_id')->references('id')->on('wall_records')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wall_record_user_exclude', function ($table) {
            $table->dropForeign('wall_record_user_exclude_wall_record_id_foreign');
        });

        Schema::dropIfExists('wall_record_user_exclude');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWallRecordImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wall_record_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wall_record_id')->unsigned();

            $table->string('path');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('wall_record_id')->references('id')->on('wall_records')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wall_record_images');
    }
}

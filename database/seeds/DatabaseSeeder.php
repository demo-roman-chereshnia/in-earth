<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /**
         * Create Users without private wall records
         */
        /** @var Collection $users */
        $users = factory(\App\User::class, 2)->create();

        $wallRecords = collect();

        /**
         * Create Users with wall records
         */
        $users = $users->merge(factory(App\User::class, 3)
            ->create()
            ->each(function (\App\User $u) use ($wallRecords) {

                /** Create Wall records by random count */
                $wallRecordCount = rand(1, 4);

                while ($wallRecordCount--) {

                    $wallRecord = factory(\App\Models\WallRecord::class)->make();

                    $u->myAuthorshipWallRecords()->save($wallRecord);

                    $wallRecords->push($wallRecord);
                }
            })
        );

        /**
         * Create random user likes on WallRecords
         */
        $wallRecords->random(rand(1, 4))->each(function ($wallRecord) use ($users) {

            $rand_user_id = $users->random(rand(1, 4))->pluck('id')->toArray();

            $wallRecord->likeUsers()->sync($rand_user_id);
        });

        /**
         * Make comments for created WallRecords
         */
        $wallRecords->each(function ($wallRecord) use ($users) {

            /** create #1 level comments */
            $comments = $this->makeComments($users, $wallRecord);
            if ($comments->count()) {
                $wallRecord->commentsRel()->saveMany($comments);
            }

            /** create #2 level comments  */
            $comments->each(function ($comment) use ($users, $wallRecord) {
                $comments = $this->makeComments($users, $wallRecord, $comment);

                if ($comments->count()) {
                    $comment->commentsRel()->saveMany($comments);
                }
            });
        });
    }

    protected function makeComments(Collection $users, \App\Models\WallRecord $record, \App\Models\Comment $commentParent = null)
    {
        $comments = collect([]);

        $count = rand(0, 3);
        while ($count--) {

            #take random user from input
            $user = $users->shuffle()->first();

            # init require columns
            $mutate = [
                "wall_record_id" => $record->id,
                "user_id" => $user->id,
            ];
            $mutate = $commentParent ? array_merge($mutate, ['parent_id' => $commentParent->id]) : $mutate;

            # make comment instance
            $comments->push(factory(\App\Models\Comment::class)->make($mutate));
        }

        return $comments;
    }
}

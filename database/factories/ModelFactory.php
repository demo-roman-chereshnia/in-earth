<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'avatar' => $faker->unique()->randomElement([
                "matt", "christian", "elliot", "jenny",
                "joe", "laura", "stevie", "veronika",
            ]) . ".jpg",
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\WallRecord::class, function (\Faker\Generator $faker) {

    $countDay = rand(1, 10);

    return [
        'text' => $faker->paragraph,
        'created_at' => $cd = $faker->dateTimeBetween('-3 month', 'now'),
        'updated_at' => $faker->randomElement([null, $cd->modify("+{$countDay} day")]),
    ];
});

$factory->define(\App\Models\Comment::class, function (\Faker\Generator $faker) {

    $countDay = rand(1, 10);

    return [
        'user_id' => null,
        'parent_id' => null,
        'wall_record_id' => null,
        'text' => $faker->paragraph,
        'created_at' => $cd = $faker->dateTimeBetween('-3 month', 'now'),
        'updated_at' => $faker->randomElement([null, $cd->modify("+{$countDay} day")]),

    ];
});
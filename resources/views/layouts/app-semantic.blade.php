<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="/media/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/media/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/media/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/media/images/favicons/manifest.json">
    <link rel="mask-icon" href="/media/images/favicons/safari-pinned-tab.svg" color="#5bbad5">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#a1c057">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#a1c057">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @stack('headerExtend')

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="libs/semantic/semantic.min.css">
    @stack('headerCSS')

    <!-- Scripts -->
    @stack('headerJS')
</head>
<body>

@yield('body')

<!-- Scripts -->
@stack('bodyJS')
</body>
</html>
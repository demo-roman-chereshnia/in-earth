@extends("layouts.app")

@push('headerCSS')
<style type="text/css">
    body > .grid {
        height: 100%;
    }

    .image {
        margin-top: -100px;
    }

    .column {
        max-width: 450px;
    }
</style>
@endpush
@push('headerJS') @endpush
@push('bodyJS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="{{ url('libs/semantic/semantic.min.js') }}"></script>
<script>
    $(function () {
        $('.checkbox').checkbox()
    });
</script>
@endpush

@section("body")
    @yield('content')
@endsection

@extends("layouts.app")

@push('headerCSS')
<style type="text/css">
    body {
        background-color: #FFFFFF;
    }

    .ui.menu .item img.logo {
        margin-right: 1.5em;
    }

    .main.container {
        margin-top: 7em;
    }

    .wireframe {
        margin-top: 2em;
    }

    .ui.footer.segment {
        margin: 5em 0em 0em;
        padding: 5em 0em;
    }
</style>
@endpush

@push('headerJS')
<script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?></script>
@if(isset($authUser))
    <script>window.auth = <?php echo json_encode($authUser ?: ((object)[])) ?></script>
@endif
<script>
    window.config = <?php echo json_encode([
        "broadcasting" => [
            "pusher" => [
                "key" => config('broadcasting.connections.pusher.key'),
                "cluster" => config('broadcasting.connections.pusher.options.cluster'),
                "encrypted" => config('broadcasting.connections.pusher.options.encrypted'),
            ]
        ]
    ]) ?>
</script>
@endpush

@push('bodyJS')
<script src="/js/app.js"></script>
<script src="{{ url('libs/moment/moment.js') }}"></script>
<script src="{{ url('libs/semantic/semantic.min.js') }}"></script>
@endpush

@section("body")
    @if(Auth::check())
        <div class="ui fixed menu">
            <div class="ui container">
                <a href="{{ route('client.wall') }}" class="header item">
                    <img class="logo" src="/media/images/logo.png">
                    {{ config('app.name') }}
                </a>
                {{--        <a href="{{ route('client.profile') }}" class="item">Profile</a>--}}

                <div class="right menu">

                    <a class="ui item" href="#"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <img class="ui right spaced avatar image" src="{{ $authUser->avatarUrl }}">
                        {{ $authUser->name }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </div>

            </div>
        </div>
    @endif

    <div class="ui main text container">
        @yield('content')
    </div>
@endsection
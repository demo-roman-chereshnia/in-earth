@extends('layouts.pages.guest')

@section('content')
    <div class="column">
        <h2 class="ui blue image header">
            <img src="media/images/logo.png" class="image">
            <div class="content">
                Register {{ config('app.name') }}
            </div>
        </h2>
        <form class="ui large form{{ $errors->count() ? ' error' : '' }}"
              role="form" method="POST" action="{{ url('/register') }}" style="text-align: left;">
            {{ csrf_field() }}

            <div class="ui stacked segment">
                <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input id="name" type="text" name="name" value="{{ old('name') }}"
                               placeholder="Your name" required autofocus>
                    </div>
                    @if ($errors->has('name'))
                        <div class="ui basic red pointing prompt label transition visible">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                    <div class="ui left icon input">
                        <i class="at icon"></i>
                        <input id="email" type="email" name="email" value="{{ old('email') }}"
                               placeholder="E-mail address" required>
                    </div>
                    @if ($errors->has('email'))
                        <div class="ui basic red pointing prompt label transition visible">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input id="password" type="password" name="password" placeholder="Password" required>
                    </div>
                    @if ($errors->has('password'))
                        <div class="ui basic red pointing prompt label transition visible">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input id="password" type="password" name="password_confirmation" placeholder="Confirm Password"
                               required>
                    </div>
                </div>

                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" tabindex="0" class="hidden" name="gravatar">
                        <label>Use <a href="https://gravatar.com">Gravatar</a> service</label>
                    </div>
                </div>

                {{--<button type="submit" class="ui fluid large blue submit button">Register</button>--}}

                <div class="ui fluid large buttons">
                    <button class="ui blue button" type="submit">Register</button>
                    <div class="or"></div>
                    <a href="{{ url('/') }}" class="ui button">Login</a>
                </div>
            </div>

        </form>
    </div>
@endsection

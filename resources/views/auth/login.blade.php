@extends('layouts.pages.guest')

@section('content')

    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui blue image header">
                <img src="media/images/logo.png" class="image">
                <div class="content">
                    Log-in {{ config('app.name') }}
                </div>
            </h2>
            <form class="ui large form{{ $errors->count() ? ' error' : '' }}"
                  role="form" method="POST" action="{{ url('/login') }}" style="text-align: left;">
                {{ csrf_field() }}

                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="at icon"></i>
                            <input id="email" type="email" name="email" value="{{ old('email') }}"
                                   placeholder="E-mail address" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <div class="ui basic red pointing prompt label transition visible">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input id="password" type="password" name="password" placeholder="Password" required>
                        </div>
                        @if ($errors->has('password'))
                            <div class="ui basic red pointing prompt label transition visible">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input type="checkbox" tabindex="0" class="hidden" name="remember">
                            <label>Remember Me</label>
                        </div>
                    </div>

                    {{--<button type="submit" class="ui fluid large blue submit button">Login</button>--}}

                    <div class="ui fluid large buttons">
                        <button class="ui blue button" type="submit">Login</button>
                        <div class="or"></div>
                        <a href="{{ url('/register') }}" class="ui button">Sign Up</a>
                    </div>
                </div>

                {{--<div class="ui error message" style="display: none;">--}}
                {{--<ul class="list">--}}
                {{--@foreach($errors->all() as $bug)--}}
                {{--<li>{{ $bug }}</li>--}}
                {{--@endforeach--}}
                {{--</ul>--}}
                {{--</div>--}}

            </form>

            {{--<div class="ui message" style="display: none;">--}}
            {{--New to us? <a href="{{ url('/register') }}">Sign Up</a>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection
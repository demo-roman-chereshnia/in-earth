@extends('layouts.pages.guest')

@push('headerCSS')
<style type="text/css">
    body {
        padding: 6em 10em;
    }
</style>
@endpush

@section('content')
    <div class="ui text container">
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Users (<i>password on default user `secret`</i>)</th>
            </tr>
            </thead>
            <tbody>

            @forelse($users as $user)
                <tr>
                    <td>
                        <h4 class="ui image header">
                            <img src="{{ $user->avatarUrl }}" class="ui mini rounded image">
                            <div class="content">
                                {{ $user->name }}
                                <div class="sub header">{{ $user->email }}</div>
                            </div>

                        </h4>
                    </td>
                </tr>
            @empty

            @endforelse

            </tbody>
        </table>
    </div>
@endsection
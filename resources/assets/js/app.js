/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Echo from "laravel-echo"

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 1000);
    });
}

window.Notifier = {
    response: function () {
        if (arguments.length) {

            var response = arguments[0],
                message = response.message;

            if (response && response.success) {

                if (response.data.cause)
                    message = message + ' [' + response.data.cause + ']'

                console.info(message)

            } else if (response && !response.success) {

                if (response.errors && response.errors.cause)
                    message = message + ' [' + response.errors.cause + ']'

                console.error(message)
            }
        }
    },
    alert: function (message) {
        console.log(message);
    }

}

Vue.component('wall-record', require('./components/WallRecordItem.vue'));
Vue.component('comment', require('./components/CommentItem.vue'));
Vue.component('like', require('./components/parts/Like.vue'));
Vue.component('not-interest', require('./components/parts/NotInterest.vue'));
Vue.component('datetime', {
    template: '<div>' +
    '<span v-if="!updated" class="date">{{ formattedDate }}</span>' +
    '<span v-if="updated" class="date">updated {{ formattedDate }}</span>' +
    '</div>',
    props: ['created', 'updated'],
    data: function () {
        return {
            formattedDate: null,
        }
    },
    created: function () {
        setInterval(this.updateDate, 10000);
        this.updateDate();
    },
    methods: {
        updateDate: function () {
            this.formattedDate = moment(this.dateView).fromNow()
            // this.formattedDate = moment(this.dateView).calendar();
            // this.formattedDate = moment(this.dateView);
        }
    },
    computed: {
        dateView: function () {
            if (this.updated) {
                return this.updated * 1000;
            }

            return this.created * 1000;
        }
    },
    filters: {
        moment: function (value) {
            return moment(value);
        }
    }
});
Vue.component('modal', require('./components/parts/ModalComponent.vue'));

new Vue({
    el: '#app',
    data: {},
    created: function () {
        this.echo = new Echo({
            broadcaster: 'pusher',
            key: window.config.broadcasting.pusher.key,
            cluster: window.config.broadcasting.pusher.cluster,
            encrypted: window.config.broadcasting.pusher.encrypted
        });
    },
    components: {
        wall: require('./components/Wall.vue')
    },
});
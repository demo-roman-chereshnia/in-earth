<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('client.wall');
Route::get('/users', 'HomeController@users')->name('client.users');

Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {

    Route::group(['prefix' => 'wall'], function () {

        Route::get('/', 'WallApiController@getWallRecords');
        Route::post('/', 'WallApiController@createWallRecord');

        Route::group(['prefix' => '{wallRecord}'], function () {
            Route::put('/', 'WallApiController@updateWallRecord');
            Route::delete('/', 'WallApiController@deleteWallRecord');
            Route::post('/interest', 'WallApiController@notInterestWallRecord');
            Route::post('/like-toggle', 'WallApiController@likeToggleWallRecord');

            Route::group(['prefix' => 'comments'], function () {

                // add child for $wallRecord or/and $comment
                Route::post('/{comment?}', 'WallCommentApiController@createWallComment');

                // work with self $comment
                Route::group(['prefix' => '{comment}'], function () {

                    Route::put('/', 'WallCommentApiController@updateWallComment');
                    Route::delete('/', 'WallCommentApiController@deleteWallComment');
                    Route::post('/like-toggle', 'WallCommentApiController@likeToggleWallComment');
                });
            });
        });
    });
});